Description
-----------
This module allow to control the visibility of a block in time duration. The
time duration can be set in number of days.

How time duration calculation works.

- No. of days (If block have the value for start date & and end date)
- Start date to Infinite time (If block have only start date value)

Why This
Using this module a block can be set as banner for display specified time
duration.

Installation
------------
1. Copy the entire block_display_control directory the Drupal sites/all/modules or
   sites/all/modules/contrib directory.

2. Login as an administrator. Enable the module in the "Administer" ->
   Modules (admin/modules)

3. Access the links to set a block as banner or display by duration.
   (admin/structure/block)

DEPENDENCIES:
-------------
block
date

AUTHOR:
-------
Devendra Yadav <dev.firoza@gmail.com>
